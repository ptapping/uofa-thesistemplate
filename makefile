# A makefile to allow complete compilation of the document by simply running
# "make" at the command line (on Linux at least...)
# The unneeded auxiliary files can be removed with "make tidy", and in addition,
# the pdf will be deleted with "make clean".

# Change this name to match that of the main document
MAINNAME = UofA-ThesisTemplate

LATEX = pdflatex -synctex=1 -interaction=nonstopmode

all : tidy main

# As building the glossary can insert page(s) and change the page numbers for
# the references (and perhaps vice-versa), this sequence should hopefully work.
main : tidy
	$(LATEX) "$(MAINNAME)"
	makeglossaries "$(MAINNAME)"
	bibtex "$(MAINNAME)"
	$(LATEX) "$(MAINNAME)"
	makeglossaries "$(MAINNAME)"
	bibtex "$(MAINNAME)"
	$(LATEX) "$(MAINNAME)"
	$(LATEX) "$(MAINNAME)"

clean : tidy
	- rm "$(MAINNAME).pdf"

tidy :
	- rm "$(MAINNAME).aux"
	- rm "$(MAINNAME).bbl"
	- rm "$(MAINNAME).blg"
	- rm "$(MAINNAME).glg"
	- rm "$(MAINNAME).glo"
	- rm "$(MAINNAME).gls"
	- rm "$(MAINNAME).lof"
	- rm "$(MAINNAME).log"
	- rm "$(MAINNAME).lot"
	- rm "$(MAINNAME).out"
	- rm "$(MAINNAME).synctex.gz"
	- rm "$(MAINNAME).tdo"
	- rm "$(MAINNAME).toc"
	- rm "$(MAINNAME).xdy"
	- rm "figures/*converted-to.pdf"

