\chapter{Introduction}
\label{sec:intro}
\glsresetall
\appendtographicspath{ {./introduction/} }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:intro:intro}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Although this template will help you produce a thesis compliant with the University of Adelaide's requirements, do not consider anything contained within as official advice.
Visit the Graduate Centre website for official program rules and specifications for your thesis at \url{https://www.adelaide.edu.au/graduatecentre/handbook/07-thesis/}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Magic is Reading the Documentation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Most of the magic for this template is contained in the \texttt{setup/preamble.tex} file.
Please use it to determine any required packages, and remember to consult the documentation for the appropriate package on the \href{http://www.ctan.org/}{Comprehensive \TeX Archive Network} for further information.

To compile this document, a \href{https://en.wikipedia.org/wiki/Makefile}{\texttt{makefile}} is provided which should automate the process on Unix-like systems (Linux, OSX).
Simply type \texttt{make} to generate the PDF, \texttt{make tidy} to remove unnecessary auxiliary files generated during compilation, and \texttt{make clean} to additionally remove the generated PDF.
The compilation process (which works, but may not be optimal) is \texttt{pdflatex}, \texttt{makeglossaries}, \texttt{bibtex}, \texttt{pdflatex}, \texttt{makeglossaries}, \texttt{bibtex}, \texttt{pdflatex}, \texttt{pdflatex}.

The document is broken up into several subdirectories and files for easy management.
The main \texttt{UofA-ThesisTemplate.tex} simply includes the various files in the desired order, as well as things like inserting \texttt{clearpage} and \texttt{cleardoublepage} commands to start chapters on the right hand page of the printed book.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Glossaries}
\label{sec:intro:glossaries}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section we see the use of the glossaries package (\url{https://www.ctan.org/pkg/glossaries}) to automatically create the \gls{loa}.
The \texttt{glsresetall} command at the start of each chapter causes the first usage to again be printed in full.
Subsequent uses of \gls{loa} will use the abbreviated form.


Note the tricks in the \texttt{preamble.tex} and the \texttt{appendtographicspath} commands at the start of each file.
This allows the images and source code for each chapter to be placed in their own separate directories, which makes things neater and easier to manage.

Placing footnotes in figure captions and getting the footnote text at the bottom of the same page as the figure can be a pain.
With this workaround we can place the \texttt{footnotemark} in the caption, and then manually place the \texttt{footnotetext} somewhere so they end up on the same page.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.5\textwidth]{cat}
    \caption[A cat]{A picture of a cat.
    This is one of many that are on the internet.\footnotemark{}
    Don't forget to put short figure captions as a square bracket option for use in the list of figures.}
    \label{fig:intro:cat}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Chemical Structures}
\label{sec:intro:structures}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% See how this footnote text is far away from the figure caption containing the footnotemark, but puts the footnote on the same page as the image.
\footnotetext{Image of cat is \copyright{} Joaquim Alves Gaspar / Wikimedia Commons / \href{http://creativecommons.org/licenses/by-sa/3.0/}{CC-BY-SA-3.0}.}

For getting the scale of chemical structures to match between figures, specifying a consistent figure size with \texttt{scale=0.95} might be better than how it's been done here with manual tweaking.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.66\textwidth]{conjpolymers}
    \caption[Structure of some conjugated polymers]{Structures of the conjugated polymers \textbf{1} polyethyne, \textbf{2} \gls{p3mt}, \textbf{3} \gls{p3ht} and \textbf{4} \gls{mehppv}.
    The structures shown in \textbf{2} and \textbf{3} are the regioregular variants, with strict head-to-tail type bonding of the monomer units.}
    \label{fig:intro:polymers}
\end{figure}

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.394\textwidth]{pedot-pss}
    \caption[Structure of the conjugated polymer PEDOT and dopant PSS]{Structures of the conjugated polymer \textbf{5} \gls{pedot} and \textbf{6} the commonly used (non-conjugated) polymer dopant \gls{pss}.}
    \label{fig:intro:pedotpss}
\end{figure}

There aren't many chemical structures here, so the labelling was done manually.
I'm sure there's some better ways or specialised \LaTeX packages to help with this.
I'm happy to integrate any good techniques that are suggested.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{A Different Section}
\label{sec:intro:different}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let's talk about something else now.
Just some more examples of basic things like inserting figures, references etc.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Diagrams and Other Stuff}
\label{sec:intro:energytransfer}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Dipolar Coupling Mechanism}
\label{sec:intro:eet}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In conjugated polymers, exciton transport occurs through a process referred to as \gls{eet}.
When chromophores are weakly coupled, as is generally the case, this occurs through discrete ``hopping'' events between chromophores.
This is a dipolar coupling mechanism governed by \gls{fret}.\cite{Forster1959}
The rate of exciton exchange between the donor and acceptor is therefore dependent on the separation distance, orientation and spectral overlap the chromophores.

\begin{figure}[htb!]
    \centering
    \includegraphics[width=0.87\textwidth]{fret}
    \caption[F\"orster resonance energy transfer mechanism]{\gls{fret} transfers energy between a donor and acceptor chromophores \textit{via} a nonradiative dipole coupling mechanism.
     The transitions on the individual donor and acceptor molecules must be spin-allowed, thus \gls{fret} usually occurs between singlet states as shown here.}
    \label{fig:intro:fret}
\end{figure}

I highly recommend using \href{https://inkscape.org}{Inkscape} for producing vector-based diagrams such as those seen here.
For the more hardcore or coding-oriented, \href{https://en.wikipedia.org/wiki/PGF/TikZ}{PGF/TikZ} can produce stunningly good diagrams.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Electron Exchange Mechanisms}
\label{sec:intro:det}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

If two molecules are in very close proximity so that there is sufficient orbital overlap, electron exchange may occur.
This requires separation distances on the order of the molecules' van der Waals radii, typically \SI{10}{\angstrom} or less. 
The transfer of an excited state through this mechanism is termed \gls{det},\cite{Dexter1953} as illustrated in Figure~\ref{fig:intro:det}.

\begin{figure}[tb!]
    \centering
    \includegraphics[width=0.87\textwidth]{det}
    \caption[Dexter excitation transfer mechanism]{\gls{det} is an electron exchange mechanism, therefore requiring the donor and acceptor to be within $\sim$\SI{10}{\angstrom} to allow sufficient orbital overlap.
    The total spin of the donor-acceptor pair must be conserved during the process; here the exchange occurs between a triplet state donor and singlet state acceptor.}
    \label{fig:intro:det}
\end{figure}

Chapters~\ref{ch:a} and \ref{ch:b} address stuff and things, respectively.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Bibliography Database}
\label{sec:intro:bib}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We will assume familiarity with the use of Bibtex reference databases.
Inside of the example \texttt{thesis.bib} Bibtex database file you will find an \texttt{control} entry type.
This contains some flags to tweak the layout of the bibliography when using the \texttt{achemso} style.
At the moment it is set to print all author names (rather than abbreviate with et al.) and include the article title.
Jabref may complain about the \texttt{control} entry type.
If so, manually add it using the \texttt{Options $\rightarrow$ Customise entry types} menu.
Additionally, it will cause \LaTeX to warn about undefined citations, which can be safely ignored.
Remember that any text in the \texttt{note} field will be appended to the reference.
This is good for including a \gls{doi} or publication status message, but usually annoying when entries have a PMID or similar.

