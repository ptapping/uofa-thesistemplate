# UofA Thesis Template #

This is a Latex template suitable for production of a thesis at the University of Adelaide. It is essentially just my PhD thesis with most of the content removed, and some instructions and examples added. While oriented towards chemistry and physics fields, it should also be applicable for use in other subject areas.

I hope it is of some use to you!

### Instructions ###

See the included compiled PDF for notes and instructions.
For a start, you will of course need a working Latex distribution!

### Contributions ###

Suggestions and contributions are welcome.
Please start an Issue, or get in touch via email.

### Contact ###

patrick dot tapping at adelaide.edu.au

### License ###
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.